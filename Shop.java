import java.util.Scanner;
public class Shop {
	public static void main(String[] args)
  {
	  final int NUMBER_PRODUCTS = 5;
	  Scanner inputs = new Scanner(System.in);
	  
	  
	  System.out.println("\n~~~~~ Welcome to PRINTER SHOP! ~~~~~");
	  //creating an array of products
	  Printer[] products = new Printer[NUMBER_PRODUCTS];
	  for (int i = 1; i < NUMBER_PRODUCTS ; i++) {
		  products[i] = new Printer();
		  System.out.println("\nWhat kind of printer would you like to buy?");
		  products[i].printerType = inputs.next();
		  System.out.println("How many papers do you need?");
		  products[i].papersP = inputs.nextInt();
		  System.out.println("note: the speed can not exceed more than 900, \n Please choose any other speed..");
		  products[i].fastSpeed = inputs.nextInt();
		  //keep trace of the sequence(index)
		  products[i].sequence = (i);
	  }

	  //last product --> last index
	  int lastIndex = products.length-1;
	  System.out.println("the Printer # " + lastIndex + ": specifications :");
	  //System.out.println("note: the number of products  ");
	  System.out.println("the index # " + lastIndex + ": the printer type is=== " + products[lastIndex].printerType);
	  System.out.println("the index # " + lastIndex + ": the printer paper # are=== " + products[lastIndex].papersP);
	  System.out.println("the index # " + lastIndex + ": the printer speed is=== " + products[lastIndex].fastSpeed);

      //calling printSpeed method and printing the last product
	  products[lastIndex].printingSpeed(); 	  
	  System.out.println("\n=============== THANK YOU! ^.^ ================");	  
  }		
}

